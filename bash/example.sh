#!/usr/bin/env bash

# This is an example of using the Photo API in bash, allowing a photo to be
# returned based on the identifier provided.
# Expects a fully-formed identifier to be provided, e.g.:
# ./example.sh wgd23@v1.person.identifiers.cam.ac.uk
# It requires curl (https://curl.se/) and jq (https://stedolan.github.io/jq/).

set -e

IDENTIFIER=$1

if [ -z $IDENTIFIER ]; then
  echo "provide an identifier in the format <value>@<scheme> as the first argument,"
  echo "for example: wgd23@v1.person.identifiers.cam.ac.uk"
  exit 1
fi

# allow the output location to be overwritten - but fallback to '-' which outputs to stdout
OUTPUT_LOCATION=${OUTPUT_LOCATION:--}

# setup variables from secrets.env
if [ -f "../secrets.env" ]; then
  source ../secrets.env
fi

# get an access token - using basic auth with the client id and secret, to
# post to the token endpoint
TOKEN_RESPONSE=$(
  curl -s -X POST --data 'grant_type=client_credentials' \
   -u $CLIENT_ID:$CLIENT_SECRET https://api.apps.cam.ac.uk/oauth2/v1/token
)

# extract the access token from the response - which will be in the format
# of {"access_token": "token", "issued_at": 1635955629911, ... }
ACCESS_TOKEN=$(echo $TOKEN_RESPONSE | jq -r '.access_token')

# query the photo API using the access token and identifier provided
curl -Ls -H "Authorization: Bearer ${ACCESS_TOKEN}" --output ${OUTPUT_LOCATION} \
  --show-error --fail \
  https://api.apps.cam.ac.uk/photo/v1beta1/approved-photos/$IDENTIFIER/content