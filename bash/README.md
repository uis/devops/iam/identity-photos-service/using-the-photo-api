# Photo API Bash sample

This directory contains an example bash script which authenticates with the API Gateway and queries
the Photo API for a photo by identifier. It makes use of `curl` to interact with the Photo API and
`jq` to parse the access token response.

## Running the example

With both [`jq`](https://stedolan.github.io/jq/) and [`curl`](https://curl.se/) installed locally,
and `../secrets.env` populated as specified in `../secrets.example.env`, the script can be run
using:

```sh
./example.sh <identifier> > example.jpeg
```

For example, the following would fetch a photo of the person with the CRSid `wgd23`:

```sh
./example.sh wgd23@v1.person.identifiers.cam.ac.uk > wgd23.jpeg
```

The example can also be run using Docker and Docker Compose, which does not require `curl` or `jq`
to be installed locally:

```sh
docker-compose run bash-example <identifier>
```

This will output the fetched photo to `./output/example.jpeg`.

If a photo cannot be found for the given person the script will exit with a non-successful exit
code and print a curl 404 error message to stderr.

## Interactions with the Photo API

The [example script](./example.sh) contains all commands used to query the Photo API and includes
comments indicating the purpose of each block. The script fetches a new access token using `curl`,
making use of the credentials defined in `../secrets.env`, and then uses this access token to
query the Photo API, again making use of `curl`. If the Photo API or access token request fail
an error will be logged and the script will exit with an unsuccessful error code.
