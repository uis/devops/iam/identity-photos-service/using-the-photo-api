# Photo API Python sample

This directory contains an example Python [FastAPI](https://fastapi.tiangolo.com/) project
which proxies to the University Photo API to retrieve approved photos of members of the University.
It handles authentication via OAuth2 Client Credentials and queries the Photo API for photos based
on CRSid, USN or Staff Number.

**IMPORTANT** this project is meant as an example of how to query the Photo API and is therefore
very simple in nature. The project itself should not be publicly deployed as doing so will lead
to potential unauthenticated access to photos of members of the University.

## Running the example

For the example to startup successfully you must create a `secrets.env` within the parent directory,
this should contains the same content as documented in `secrets.example.env`.

This API requires either Python >= 3.9 or Docker and Docker Compose to run locally.

If Python 3.9 is installed locally the project can be run using:

```sh
# we recommended creating a python virtual environment before installing requirements:
# https://docs.python.org/3/tutorial/venv.html
pip install -r requirements.txt
uvicorn main:app --reload --port 3000
```

If Docker and Docker Compose are installed locally the project can be run using:

```sh
docker-compose up
```

The API can then be used by making a GET request to `http://localhost:3000`.

## Interaction with the Photo API

All code is contained within [main.py](./main.py), and hopefully is usefully commented to
indicate the purpose of each block. A photo is fetched within the `get_photo_by_identifier`
function in [main.py](./main.py), this uses [requests](https://pypi.org/project/requests/) to
query the Photo API based on an identifier, using the access token generated using a
[BackendApplicationClient](https://oauthlib.readthedocs.io/en/latest/oauth2/clients/backendapplicationclient.html?highlight=BackendApplicationClient) provided by
[OAuthLib](https://oauthlib.readthedocs.io/en/latest/).
