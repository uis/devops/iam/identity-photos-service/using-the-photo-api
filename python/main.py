import os
from fastapi import FastAPI, Response
from fastapi.responses import PlainTextResponse
from dotenv import load_dotenv
from httpx import HTTPStatusError
from authlib.integrations.httpx_client import AsyncOAuth2Client
from time import time

app = FastAPI()

# load our secret values from secrets.env
load_dotenv("../secrets.env")

# set up our constants which we expect to be on the environment
CLIENT_ID = os.environ.get('CLIENT_ID')
CLIENT_SECRET = os.environ.get('CLIENT_SECRET')
TOKEN_HOST = os.environ.get('TOKEN_HOST', 'https://api.apps.cam.ac.uk')
TOKEN_PATH = os.environ.get('TOKEN_PATH', '/oauth2/v1/token')
PHOTO_API_BASE = os.environ.get('PHOTO_API_BASE', 'https://api.apps.cam.ac.uk/photo')

# we need a client id and client secret to run the app - if this error throws make sure
# secrets.env is properly populated
if not CLIENT_ID or not CLIENT_SECRET:
  raise ValueError('CLIENT_ID and CLIENT_SECRET must be provided on the environment')

# a holder for our http client which we use to communicate with the Photo API.
http_client = AsyncOAuth2Client(CLIENT_ID, CLIENT_SECRET)


# when the server shuts down we want to close our http client.
@app.on_event("shutdown")
async def shutdown_event():
  await http_client.close()


async def get_http_client():
  """
  A helper method to get an http client session which ensures that the oauth2 access token
  is always populated and has not expired.

  """
  if not http_client.token or time() >= http_client.token.get('expires_at'):
    await http_client.fetch_token(f'{TOKEN_HOST}{TOKEN_PATH}')
  return http_client


async def get_photo_by_identifier(identifier: str):
  """
  Queries the Photo API by id making use of the request session created above.
  Returns the content type and the content of the response.

  """
  client = await get_http_client()
  response = await client.get(
    f'{PHOTO_API_BASE}/v1beta1/approved-photos/{identifier}/content',
    follow_redirects=True
  )
  response.raise_for_status()
  return response.headers['content-type'], response.content


@app.get('/', response_class=PlainTextResponse)
def read_root():
  return (
    'Make a request to /crsid/:crsid to view a photo by crsid,'
    ' or /staff-number/:staffNumber to view a photo by staff number,'
    ' or /usn/:usn to view a photo by USN (student number)'
  )


@app.get('/crsid/{crsid}')
async def read_photo_by_crsid(crsid: str):
  try:
    content_type, data = await get_photo_by_identifier(
      f'{crsid}@v1.person.identifiers.cam.ac.uk'
    )
    return Response(data, media_type=content_type)
  except HTTPStatusError as e:
    return PlainTextResponse(f'Photo API returned error: {e}', status_code=e.response.status_code)


@app.get('/staff-number/{staff_number}')
async def read_photo_by_staff_number(staff_number: str):
  try:
    content_type, data = await get_photo_by_identifier(
      f'{staff_number}@person.v1.human-resources.university.identifiers.cam.ac.uk'
    )
    return Response(data, media_type=content_type)
  except HTTPStatusError as e:
    return PlainTextResponse(f'Photo API returned error: {e}', status_code=e.response.status_code)


@app.get('/usn/{usn}')
async def read_photo_by_usn(usn: str):
  try:
    content_type, data = await get_photo_by_identifier(
      f'{usn}@person.v1.student-records.university.identifiers.cam.ac.uk'
    )
    return Response(data, media_type=content_type)
  except HTTPStatusError as e:
    return PlainTextResponse(f'Photo API returned error: {e}', status_code=e.response.status_code)
