# Photo API NodeJS sample

This directory contains an example NodeJS [Express](https://www.npmjs.com/package/express) project
which proxies to the University Photo API to retrieve approved photos of members of the University.
It handles authentication via OAuth2 Client Credentials and queries the Photo API for photos based
on CRSid, USN or Staff Number.

**IMPORTANT** this project is meant as an example of how to query the Photo API and is therefore
very simple in nature. The project itself should not be publicly deployed as doing so will lead
to potential unauthenticated access to photos of members of the University.

## Running the example

For the example to startup successfully you must create a `secrets.env` within the parent directory,
this should contains the same content as documented in `secrets.example.env`.

This API requires either Node >= 16 or Docker and Docker Compose to run locally.

If Node 16 is installed locally the project can be run using:

```sh
npm install
npm start
```

If Docker and Docker Compose are installed locally the project can be run using:

```sh
docker-compose up
```

The API can then be used by making a GET request to `http://localhost:3000`.

## Interaction with the Photo API

All code is contained within [index.js](./index.js), and hopefully is usefully commented to
indicate the purpose of each block. A photo is fetched within the `getPhoto` function in
[index.js](./index.js), this uses [node-fetch](https://www.npmjs.com/package/node-fetch) to
query the Photo API based on an identifier, using the access token generated using
[simple-oauth2](https://www.npmjs.com/package/simple-oauth2).
