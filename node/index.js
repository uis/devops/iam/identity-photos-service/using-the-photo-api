import express from 'express';
import dotenv from 'dotenv';
import fetch from 'node-fetch';
import { ClientCredentials } from 'simple-oauth2'

const app = express()

// pull secrets from our secrets.env file in the outer directory
dotenv.config({ path: '../secrets.env' });

const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

// check that we have the credentials needed to query the Photo API
if (!clientId || !clientSecret) {
  throw new Error('Must provide a CLIENT_ID and CLIENT_SECRET on environment')
}

// a client for requesting access tokens from the API Gateway - see:
// https://www.npmjs.com/package/simple-oauth2
const oauth2Client = new ClientCredentials({
  client: {
    id: process.env.CLIENT_ID,
    secret: process.env.CLIENT_SECRET
  },
  auth: {
    tokenHost: process.env.TOKEN_HOST || 'https://api.apps.cam.ac.uk',
    tokenPath: process.env.TOKEN_PATH || '/oauth2/v1/token'
  }
});

// a holder for our access token and a method to get or refresh our access token if needed
let _accessToken = null;
const getToken = async () => {
  if (_accessToken === null || _accessToken.expired()) {
    // the below method fetches a bearer token that we can use to query the Photo API
    _accessToken = await oauth2Client.getToken();
  }
  return _accessToken.token.access_token;
};

/**
 * Queries the Photo API for a given photo identifier - which should be in the format of
 * <identifier>@<identifier_scheme>, e.g. wgd23@v1.person.identifiers.cam.ac.uk.
 * @returns Returns an array containing:
 *  - the Photo API's status code,
 *  - the content type of the image returned,
 *  - the body of the response, which will be the image bytes if the request was successful,
 */
const getPhoto = async (photoIdentifier) => {
  const photoAPIBase = process.env.PHOTO_API_BASE || 'https://api.apps.cam.ac.uk/photo';

  // get our access token using the OAuth2 client
  const accessToken = await getToken();
  // make a request to the Card API, using the access token in the Authorization header
  const response = await fetch(
    `${photoAPIBase}/v1beta1/approved-photos/${photoIdentifier}/content`, {
      headers: { 'Authorization': `Bearer ${accessToken}` }
    }
  );

  if (response.ok) {
    return [response.status, response.headers.get('content-type'), await response.buffer()]
  } else {
    return [
      response.status,
      response.headers.get('content-type'),
      `Photo API responded with ${response.statusText}`
    ]
  }
};

// the base route - gives some information about the routes available
app.get('/', (req, res) => {
  res.send(
    `Make a request to /crsid/:crsid to view a photo by crsid,` +
    ` or /staff-number/:staffNumber to view a photo by staff number,` +
    ` or /usn/:usn to view a photo by USN (student number)`
  );
});

// the route that allows querying by crsid
app.get('/crsid/:crsid', async (req, res) => {
  try {
    const [statusCode, contentType, body] = await getPhoto(`${req.params.crsid}@v1.person.identifiers.cam.ac.uk`);
    res.contentType(contentType).set("Content-Disposition", "inline").status(statusCode).send(body);
  } catch (error) {
    res.status(500).send(error);
  }
});

// the route that allows querying by staff number
app.get('/staff-number/:staffNumber', async (req, res) => {
  try {
    const [statusCode, contentType, body] = await getPhoto(`${req.params.staffNumber}@person.v1.human-resources.university.identifiers.cam.ac.uk`);
    res.contentType(contentType).set("Content-Disposition", "inline").status(statusCode).send(body);
  } catch (error) {
    res.status(500).send(error);
  }
});

// the route that allows querying by usn
app.get('/usn/:usn', async (req, res) => {
  try {
    const [statusCode, contentType, body] = await getPhoto(`${req.params.usn}@person.v1.student-records.university.identifiers.cam.ac.uk`);
    res.contentType(contentType).set("Content-Disposition", "inline").status(statusCode).send(body);
  } catch (error) {
    res.status(500).send(error);
  }
});

const port = parseInt(process.env.PORT || '3000', 10);
app.listen(port, () => console.log(`Photo API sample client running at http://localhost:${port}`));
