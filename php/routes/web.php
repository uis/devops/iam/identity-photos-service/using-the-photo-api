<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

// base route with information about how to use the API
$router->get('/', function() {
    return (
        'Make a request to /crsid/:crsid to view a photo by crsid,' .
        ' or /staff-number/:staffNumber to view a photo by staff number,' .
        ' or /usn/:usn to view a photo by USN (student number)'
    );
});

// route to fetch a photo by crsid
$router->get('/crsid/{crsid}', function($crsid) use ($router) {
    return $router->app->call('App\Http\Controllers\PhotoAPIController@getPhoto', [
        'identifier' => $crsid . '@v1.person.identifiers.cam.ac.uk'
    ]);
});

// route to fetch a photo by staff number
$router->get('/staff-number/{staffNumber}', function($staffNumber) use ($router) {
    return $router->app->call('App\Http\Controllers\PhotoAPIController@getPhoto', [
        'identifier' => $staffNumber . '@person.v1.human-resources.university.identifiers.cam.ac.uk'
    ]);
});

// route to fetch a photo by student number (USN)
$router->get('/usn/{usn}', function($usn) use ($router) {
    return $router->app->call('App\Http\Controllers\PhotoAPIController@getPhoto', [
        'identifier' => $usn . '@person.v1.student-records.university.identifiers.cam.ac.uk'
    ]);
});
