# Photo API PHP sample

This directory contains an example PHP [Lumen](https://lumen.laravel.com/) project which proxies
to the University Photo API to retrieve approved photos of members of the University. It handles
authentication via OAuth2 Client Credentials and queries the Photo API for photos based on CRSid,
USN or Staff Number.

**IMPORTANT** this project is meant as an example of how to query the Photo API and is therefore
very simple in nature. The project itself should not be publicly deployed as doing so will lead
to potential unauthenticated access to photos of members of the University.

## Running the example

For the example to startup successfully you must create a `secrets.env` within the parent directory,
this should contains the same content as documented in `secrets.example.env`.

This API requires either PHP >= 7.3 and [composer](https://getcomposer.org/) >= 2.1 or Docker and
Docker Compose to run locally.

If PHP 7.3 and composer are installed locally the project can be run using:

```sh
composer install
php -S 0.0.0.0:3000 -t public
```

If Docker and Docker Compose are installed locally the project can be run using:

```sh
docker-compose up
```

The API can then be used by making a GET request to `http://localhost:3000`.

## Interaction with the Photo API

The Photo API is invoked by the [`PhotoAPIService`](./app/Http/Services/PhotoAPIService.php) which
makes use of [Guzzle](https://docs.guzzlephp.org/en/stable/index.html) to query the API, and
[Guzzle OAuth2 Subscriber](https://github.com/kamermans/guzzle-oauth2-subscriber) to authenticate.
The [`PhotoAPIController`](./app/Http/Controllers/PhotoAPIController.php) makes use of the
`PhotoAPIService` to return the image response to the user or print and error if the Photo API
has responded with an error. The `PhotoAPIController` itself is invoked from the router defined
in [php/routes/web.php](php/routes/web.php).
