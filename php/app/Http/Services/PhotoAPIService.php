<?php

namespace App\Http\Services;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\OAuth2Middleware;


class PhotoAPIService
{
  public function __construct()
  {
    // create a client for fetching an access token
    $authClient = new Client([
      # specify the token uri - this defaults to https://api.apps.cam.ac.uk/oauth2/v1/token
      'base_uri' => env('TOKEN_HOST', 'https://api.apps.cam.ac.uk') . env('TOKEN_PATH', '/oauth2/v1/token'),
    ]);
    // create the oauth2 middleware which will handle fetching access tokens
    $oauth = new OAuth2Middleware(new ClientCredentials($authClient, [
      "client_id" => env('CLIENT_ID'),
      "client_secret" => env('CLIENT_SECRET'),
    ]));

    // create a request handler stack which includes our oauth2 middleware
    $stack = HandlerStack::create();
    $stack->push($oauth);

    // create a client for accessing the Photo API
    $this->photoAPIClient = new Client([
      'handler' => $stack,
      'auth' => 'oauth',
      'http_errors' => false,
      'base_uri' => env('PHOTO_API_BASE', 'https://api.apps.cam.ac.uk/photo/'),
    ]);
  }

  /**
   * Returns an http response for a request to get a photo's content from the Photo API
   * based on the photo identifier provided.
   */
  public function getPhotoContent($identifier)
  {
    return $this->photoAPIClient->get('v1beta1/approved-photos/' . $identifier . '/content');
  }
}