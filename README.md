# Using the Photo API

The [University Photo API](https://developer.api.apps.cam.ac.uk/docs/photo/1/overview) can be used
to fetch photos of members of the university which have been reviewed and determined to be a good
representation of a given person. The following gives an overview of how to fetch photo content
from the University Photo API,
[the API documentation](https://developer.api.apps.cam.ac.uk/docs/photo/1/overview) should be
consulted for an overview of all endpoints available on the University Photo API.

## Fetching the content of an approved photo:

The most useful endpoint exposed by the University Photo API is the `approved-photo-content`
endpoint which returns the most recently approved photo of a member of the university, it can be
queried by making a `GET` request to:

```
https://api.apps.cam.ac.uk/photo/v1beta1/approved-photos/<identifier>/content
```

For example:

```
https://api.apps.cam.ac.uk/photo/v1beta1/approved-photos/wgd23@v1.person.identifiers.cam.ac.uk/content
```

If no approved photos can be found of the given person, a 404 response will be returned.

The identifier schemes supported by the Photo API are:

* CRSid - as `<value>@v1.person.identifiers.cam.ac.uk`
* Staff Number - as `<value>@person.v1.human-resources.university.identifiers.cam.ac.uk`
* USN (Student Number) - as `<value>@person.v1.student-records.university.identifiers.cam.ac.uk`

For example, to query for a photo of someone with the USN `2000` we can make the following GET
request:

```
https://api.apps.cam.ac.uk/photo/v1beta1/approved-photos/2000@person.v1.student-records.university.identifiers.cam.ac.uk/content
```

## Authentication

The University Photo API requires the use of
[OAuth2 Client Credentials](https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/)
authentication. This involves a set of client credentials being exchanged with the API Gateway
for a short-lived access token which can be used to query the University Photo API.

The API Gateway's developer portal
[contains documentation about how to gain access to client credentials](https://developer.api.apps.cam.ac.uk/start-using-an-api),
as well as an overview of how to make use of the OAuth2 Client Credentials flow.

## Code samples

This repository contains code samples which make use of the Photo API in a number of different
languages. The simplest example is in `bash` which simply exchanges the client credentials for
an access token and then queries the Photo API for a photo's content. The `node`, `php`, and
`python` examples all create a server which acts as a proxy to the University Photo API, allowing
access to photos by CRSid, staff number or usn. Each sample contains a readme giving an overview
of how it is implemented:

* [bash](./bash/README.md)
* [node](./node/README.md)
* [php](./php/README.md)
* [python](./python/README.md)

**Important** All of these examples are illustrative and simply demonstrate how the Photo API can
be queried from code. They are not intended for production use and should not be deployed to
environments where they can be accessed without authentication.

### Prerequisites

All of the samples have been dockerized and contain a readme with instructions for running the
sample either via docker-compose or without docker with local dependencies.

All samples rely on `secrets.env` being populated with client credentials generated within the API
Gateway in the format given in `secrets.example.env`. A guide to generating the credentials
required for `secrets.env` can be found
[on the API Gateway's developer portal](https://developer.api.apps.cam.ac.uk/start-using-an-api).
Client credentials used in `secrets.env` should be granted access to the University Photo API
within the API Gateway's developer portal.
